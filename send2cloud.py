from flask import Flask, request, redirect, session, render_template, make_response
from flask.ext.login import LoginManager, login_required, login_user, logout_user, current_user
from flask.ext.babel import Babel
from my_classes.my_forms import LoginForm, RegisterForm, ChangePasswordForm, SetSecurityCode, EmailForm, DropboxCodeForm
from my_classes import MyDropboxClass, EmailController
from dao import *
from my_classes.func import get_hash_name, str_to_sha
from s2csettings import LANGUAGES


app = Flask(__name__)
app.secret_key = "hdfghleylergjrleifherkjflwdwkljflwej"
app.config.from_object('s2csettings')
babel = Babel(app)


login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'index'


@babel.localeselector
def get_locale():
    try:
        return session['lang']
    except:
        return request.accept_languages.best_match(LANGUAGES.keys())


@login_manager.user_loader
def load_user(userid):
    return get_user_byId(userid)


@app.route('/')
@app.route('/index')
def index():
    if request.args.get('next'):
        login_error = True
    else:
        login_error = False
    return render_template('index.html', login_error=login_error)


@app.route('/register', methods=['POST'])
def register():
    register_form = RegisterForm(request.form)
    if request.method == "POST" and register_form.validate():
        email = EmailController()
        my_hash = get_hash_name()
        hash_link = "http://send2cloud.com/auth?hashlink=" + my_hash
        new_user = add_user(register_form.user.data, register_form.password.data, register_form.email.data)
        add_auth_hash(new_user.user_id, my_hash)
        # Create the body of the message (a plain-text and an HTML version).
        text = "Hi!\nHere is the activation link: " + hash_link
        html = """\
        <html>
          <head></head>
          <body>
            <p>Hi!<br>
               Here is the activation link: """ + hash_link + """
            </p>
          </body>
        </html>
        """
        email.sendMail(request.form['email'], 'Activation link for send2cloud.com', text, html)
        resp = make_response(render_template('message.html', auth_link=hash_link, message_text="We've sent a message to your email. Please check it and follow the link from letter"))
        return resp
    else:
        return render_template('index.html')


@app.route('/auth')
def auth():
    my_hash = request.args.get('hashlink')
    if my_hash:
        result = get_saved_hash(my_hash)
        if result:
            return redirect('/')
        else:
            resp = make_response(render_template('message.html', message_text="We don't now anything about you!"))
        return resp
    else:
        return redirect('/')


@app.route('/login', methods=['POST'])
def login():
    login_form = LoginForm(request.form)
    if login_form.validate:
        login = login_form.user.data
        password = str_to_sha(login_form.password.data)
        print password
        user = get_user(login, password)
        if user:
            login_user(user)
            return redirect("/user")
        else:
            return redirect('/index?next=1')


@app.route('/user')
@login_required
def user():
    user = current_user
    clouds = get_all_user_clouds_logo(user.user_id)
    data = get_stats(user.user_id)
    return make_response(render_template('cabinet_main.html', clouds=clouds, data=data))


@app.route('/account')
@login_required
def account():
    return make_response(render_template('cabinet_account.html'))


@app.route('/blacklist')
@login_required
def blacklist():
    black_list = get_user_blacklist(current_user.user_id)
    if black_list:
        return make_response(render_template('cabinet_blacklist.html', black_list=black_list))
    else:
        return make_response(render_template('cabinet_blacklist.html'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect("/")


@app.route('/adddropbox')
@login_required
def send_req_DropboxCloud():
    user = current_user
    my_box = MyDropboxClass(user.user_id)
    auth_link = my_box.MakeAppAuthReq(user.user_id)
    resp = make_response(render_template('message.html', auth_link=auth_link,
                                         message_text="Now you should go to the Cloud page with the link below. Please authorize our application there. Then return and enter code given by Dropbox",
                                         inputCode=True))

    return resp

@app.route('/recover')
def recover():
    resp = make_response(render_template('message.html',
                                         message_text="Please enter the email, that was used for registration",
                                         inputMail=True))
    return resp


@app.route('/send_reminder', methods=['POST'])
def send_rem_mail():
    email_form = EmailForm(request.form)
    if request.method == "POST" and email_form.validate():
        this_user = get_user_byEmail(email_form.email.data)
        my_hash = get_hash_name()
        if this_user:
            hash_link = "http://send2cloud.com/restore?hashlink=" + my_hash
            add_auth_hash(this_user.user_id, my_hash)
            # Create the body of the message (a plain-text and an HTML version).
            text = "Hi!\nHere is the link to restore your password: " + hash_link
            html = """\
            <html>
              <head></head>
              <body>
                <p>Hi!<br>
                   Here is the link to restore your password: """ + hash_link + """
                </p>
              </body>
            </html>
            """
            email = EmailController()
            email.sendMail(request.form['email'], 'Password recovery for send2cloud.com', text, html)
    return redirect('/')

#after user got restore password link
@app.route('/restore')
def restore():
    my_hash = request.args.get('hashlink')
    if my_hash:
        result = get_saved_hash(my_hash)
        if result:
            resp = make_response(render_template('message.html', message_text="Please, enter your new password", passRestore=True))
            session['user_name'] = result
            return resp
        else:
            return redirect('/index')
    else:
        return redirect('/index')


#after user got restore password link
@app.route('/setnewpass', methods=['POST'])
@login_required
def set_new_password():
    change_form = ChangePasswordForm(request.form)
    if 'user_name'in session and request.method == "POST" and change_form.validate():
        change_password(session['user_name'], change_form.password2.data)
    return redirect('/')


#after user got restore password link
@app.route('/chgpassword', methods=['POST'])
@login_required
def chg_password():
    change_form = ChangePasswordForm(request.form)
    if request.method == "POST" and change_form.validate():
        print change_form.password2.data
        user = get_user_byId(current_user.user_id)
        change_password(user.user_name, change_form.password2.data)
    return redirect('/user')


#set session for preferred language
@app.route('/changelang/<lang>')
def chg_lang(lang):
    session['lang'] = lang
    return redirect('/')


@app.route('/setcode', methods=['POST'])
@login_required
def set_code():
    code_form = SetSecurityCode(request.form)

    if request.method == "POST" and code_form.validate():
        set_secur_code(current_user.user_id, code_form.code.data)
    return redirect('/user')


@app.route('/gotdropbox', methods=['POST'])
@login_required
def got_answer_DropboxCloud():
    dropboxcode_form = DropboxCodeForm(request.form)
    if request.method == "POST" and dropboxcode_form.validate():
        user = current_user
        my_box = MyDropboxClass(user.user_id)
        user_token = my_box.GotCode(dropboxcode_form.code.data)
        cloud = get_current_cloud("dropbox")
        add_cloud_to_user(int(user.user_id), user_token, cloud)
    return redirect("/user")


@app.route('/add_tobl')
@login_required
def add_black_list():
    email = request.args.get('email')
    user = current_user
    add_email_to_blacklist(email, user.user_id)
    return redirect('/user')

@app.route('/removebl')
@login_required
def remove_black_list():
    email = request.args.get('email')
    user = current_user
    remove_email_from_blacklist(email, user.user_id)
    return redirect('/blacklist')

#TODO del in release
@app.route('/install')
def install():
    CreateDB()
    return redirect("/")


if __name__ == '__main__':
    app.run(debug=True)
