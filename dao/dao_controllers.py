from sqlalchemy import create_engine, func, distinct
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from dao import User, UserCloud, Clouds, AuthHash, Files, BlackList
from datetime import date

__author__ = 'krylov'


def create_session():
    Base = declarative_base()
    engine = create_engine('mysql://root:aoSHadw82@127.0.0.1:3306/sendtocloud?charset=utf8', encoding='utf8')
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


def add_user(user_name, user_password, user_email):
    session = create_session()
    new_user = User(user_name, user_password, user_email)
    session.add(new_user)
    session.commit()
    session.refresh(new_user)
    session.close()
    return new_user


def add_auth_hash(user, hash_link):
    session = create_session()
    new_auth_hash = AuthHash(hash_link, date.today(), user)
    session.add(new_auth_hash)
    session.commit()
    session.close()


def get_saved_hash(got_hash):
    session = create_session()
    new_auth_hash = session.query(AuthHash).filter(AuthHash.auth_hash == got_hash).first()
    if new_auth_hash:
        user_name = ''
        this_user = session.query(User).filter(User.user_id == new_auth_hash.users.user_id).first()
        this_user.user_active = True
        user_name = this_user.user_name
        session.delete(new_auth_hash)
        session.commit()
        session.close()
        return user_name
    else:
        return False


def change_password(user_name, new_password):
    session = create_session()
    this_user = session.query(User).filter(User.user_name == user_name).first()
    this_user.user_password = new_password
    session.commit()
    session.close()


def get_user(user_name, user_password):
    session = create_session()
    result = session.query(User).filter(User.user_name.like(user_name)).filter(
        User.user_password.like(user_password)).first()
    session.close()
    return result


def get_user_byId(user_id):
    session = create_session()
    result = session.query(User).filter(User.user_id == user_id).first()
    session.close()
    return result


def get_user_byName(user_name):
    session = create_session()
    result = session.query(User).filter(User.user_name.like(user_name)).first()
    session.close()
    return result


def get_user_byEmail(user_email):
    session = create_session()
    result = session.query(User).filter(User.user_email == user_email).first()
    session.close()
    return result


def add_cloud_to_user(user_id, token, cloud):
    session = create_session()
    user_cloud = UserCloud(int(user_id), token, int(cloud))
    session.add(user_cloud)
    session.commit()
    session.close()


def get_current_cloud(name):
    session = create_session()
    result = session.query(Clouds).filter(Clouds.cloud_name == name).first()
    session.close()
    return result.cloud_id


def get_all_clouds():
    session = create_session()
    result = session.query(Clouds).all()
    session.close()
    return result


def get_dropbox_apptoken():
    session = create_session()
    result = session.query(Clouds).filter(Clouds.cloud_name == "dropbox").first()
    session.close()
    return result


def get_all_user_clouds_logo(user_id):
    session = create_session()
    clouds = session.query(UserCloud).filter(UserCloud.user_id == user_id).all()
    result = []
    for cloud in clouds:
        result.append(cloud.clouds.cloud_logo)
    session.close()
    return result


def get_stats(user_id):
    count = 0
    size = 0
    from_mails = 0
    session = create_session()
    files = session.query(Files).filter(Files.user_id == user_id).all()
    if len(files) > 0:
        from_mails = session.query(func.count(distinct(Files.file_from_email))).first()
        for file in files:
            count += 1
            size += file.file_size
        result = {'count': count, 'size': round((float(size) / 1024) / 1024, 2), 'from': int(from_mails[0]),
                  'files': files}

    else:
        result = {'count': 0, 'size': 0, 'from': '', 'files': ''}

    session.close()
    return result


def get_user_token_from_db(user_id, cloud_name):
    session = create_session()
    user_clouds = session.query(UserCloud).filter(UserCloud.user_id == user_id).join(UserCloud.clouds). \
        filter(Clouds.cloud_name == cloud_name).all()
    if user_clouds:
        return user_clouds[0].user_cloud_token
    else:
        False


def add_file_to_db(user_id, file_name, file_size, file_from_email, file_date):
    session = create_session()
    new_file = Files(user_id, file_name, file_size, file_from_email, file_date)
    session.add(new_file)
    session.commit()
    session.close()


def add_email_to_blacklist(blacklist_email, blacklist_got_from):
    session = create_session()
    new_black_mail = BlackList(blacklist_email, blacklist_got_from, date.today())
    files_from_blackmail = session.query(Files).filter(Files.file_from_email == blacklist_email).all()
    for file in files_from_blackmail:
        session.delete(file)
    session.add(new_black_mail)
    session.commit()
    session.close()


def set_secur_code(user_id, code):
    session = create_session()
    user = get_user_byId(user_id)
    session.query(User).filter(User.user_id == user_id).update({User.user_secret: code})
    session.commit()
    session.close()


def remove_email_from_blacklist(blacklist_email, blacklist_got_from):
    session = create_session()
    blackmail_list = session.query(BlackList).filter(BlackList.blacklist_email == blacklist_email,
                                                     BlackList.user_id == blacklist_got_from).all()
    for file in blackmail_list:
        session.delete(file)
    session.commit()
    session.close()


def test_for_blacklist(email, user_id):
    """

    :param email:
    :param user_id:
    :return: True if email is in black list
    """
    session = create_session()
    black_item = session.query(User).filter(User.user_id == user_id).join(BlackList).filter(BlackList.blacklist_email == email).first()
    if black_item:
        return True
    else:
        return False


def get_user_blacklist(user_id):
    session = create_session()
    black_list = session.query(BlackList).filter(BlackList.user_id == user_id).all()
    if len(black_list) > 0:
        return black_list
    else:return False