import datetime

__author__ = 'krylov'
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, create_engine, ForeignKey, Boolean, SmallInteger
from sqlalchemy.orm import relationship, backref, sessionmaker



Base = declarative_base()

engine = create_engine('mysql://root:aoSHadw82@127.0.0.1:3306/sendtocloud', encoding='utf8')


class User(Base):
    __tablename__ = 'users'
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }
    user_id = Column(Integer, primary_key=True)
    user_name = Column(String(100), nullable=False, unique=True)
    user_password = Column(String(70, collation='utf8_bin'), nullable=False)
    user_clouds = relationship("UserCloud", backref="users")
    hash_links = relationship("AuthHash", backref="users")
    files = relationship("Files", backref="users")
    user_email = Column(String(50), unique=True)
    user_secret = Column(SmallInteger, default=None)
    user_active = Column(Boolean, default=False)
    black_list = relationship('BlackList', backref='users')
    is_premium = Column(Boolean, default=False)

    def __init__(self, user_name, user_password, user_email):
        self.user_name = user_name
        self.user_password = user_password
        self.user_email = user_email



    def is_active(self):
        return True

    def get_id(self):
            return self.user_id

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False


class UserCloud(Base):
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }
    __tablename__ = 'users_cloud'
    users_cloud_id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.user_id'), nullable=False)
    user_cloud_token = Column(String(70), nullable=False)
    cloud_id = Column(Integer, ForeignKey('clouds.cloud_id'))

    def __init__(self, user_id, user_cloud_token, cloud_id):
        self.user_id = user_id
        self.user_cloud_token = user_cloud_token
        self.cloud_id = cloud_id


class Clouds(Base):
    __tablename__ = 'clouds'
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }
    cloud_id = Column(Integer, primary_key=True)
    cloud_name = Column(String(30), nullable=False)
    cloud_app_key = Column(String(30), nullable=False)
    cloud_app_secret = Column(String(30), nullable=False)
    cloud_logo = Column(String(30), nullable=False)
    user_cloud = relationship("UserCloud", backref="clouds")

    def __init__(self, cloud_name, cloud_app_key, cloud_app_secret, cloud_logo):
        self.cloud_name = cloud_name
        self.cloud_app_key = cloud_app_key
        self.cloud_app_secret = cloud_app_secret
        self.cloud_logo = cloud_logo


class BlackList(Base):
    __tablename__ = 'blacklist'
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }
    blacklist_id = Column(Integer, primary_key=True)
    blacklist_email = Column(String(30), nullable=False)
    blacklist_date = Column(Date, nullable=False)
    user_id = Column(Integer, ForeignKey('users.user_id'))

    def __init__(self, blacklist_email, user_id, blacklist_date):
        self.blacklist_email = blacklist_email
        self.user_id = user_id
        self.blacklist_date = blacklist_date


class AuthHash(Base):
    __tablename__ = 'auth_hash'
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }
    auth_hash_id = Column(Integer, primary_key=True)
    auth_hash = Column(String(33), nullable=False)
    auth_hash_date = Column(Date, nullable=False)
    user_id = Column(Integer, ForeignKey('users.user_id'))

    def __init__(self, auth_hash, auth_hash_date, user_id):
        self.auth_hash = auth_hash
        self.auth_hash_date = auth_hash_date
        self.user_id = user_id


class Files(Base):
    __tablename__ = 'files'
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }
    file_id = Column(Integer, primary_key=True)
    file_name = Column(String(100), nullable=False)
    file_size = Column(Integer, nullable=False)
    file_from_email = Column(String(100), nullable=False)
    user_id = Column(Integer, ForeignKey('users.user_id'))
    file_date = Column(Date, nullable=False)

    def __init__(self, user_id, file_name, file_size, file_from_email, file_date):
        self.user_id = user_id
        self.file_name = file_name
        self.file_size = file_size
        self.file_from_email = file_from_email
        self.file_date = file_date


#initial setup for DB
def CreateDB():
    Base.metadata.create_all(engine)
    app_key = 'ydpvi3t45khch05'
    app_secret = 'xeoxgqd6fp9stzx'
    Dropbox = Clouds("dropbox", app_key, app_secret, "dropbox.png")
    Session = sessionmaker(bind=engine)
    session = Session()
    session.add(Dropbox)
    session.commit()
    session.close()
