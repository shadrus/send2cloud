import time, datetime
from dao import get_user_byName, get_user_token_from_db, add_file_to_db
from my_classes import EmailController, MyDropboxClass
import logging
from operator import itemgetter

from daemon import runner


__author__ = 'krylov'


#logging.basicConfig(filename='s2c.log')
class App():
    def __init__(self):
        #self.stdin_path = '/home/shadrus/Projects/send2cloud/'
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/tmp/foo.pid'
        self.pidfile_timeout = 5

    def start_check_mail(self):
        logging.info('Checking mail...')
        new_mail = []
        mail = EmailController()
        count = mail.look_for_email()
        if count > 0:
            new_mail = mail.get_email_list()
            if len(new_mail) > 0:
                new_mail = sorted(new_mail, key=itemgetter('is_premium'), reverse=True)
                self.send_to_dropbox(new_mail)

    def send_to_dropbox(self, new_mail):
        for item in new_mail:
            user_id = get_user_byName(item['to']).user_id
            if get_user_token_from_db(user_id, 'dropbox'):
                user_box = MyDropboxClass(user_id)
                if user_box.putFile(item['attach_name'], item['hash_name'], True):
                    add_file_to_db(user_id, item['attach_name'], item['filesize'], item['from'], datetime.date.today())
            else:
                pass

    def run(self):
        while True:
            print 'Starting...'
            self.start_check_mail()
            time.sleep(10)

app = App()
daemon_runer = runner.DaemonRunner(app)
daemon_runer.do_action()

'''
if __name__ == "__main__":
    run()
'''







