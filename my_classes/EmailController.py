# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import decode_header
import logging
import smtplib
import poplib as pop
from dao import get_user_byName, test_for_blacklist, get_user_byId
from my_classes.func import get_hash_name, from_koi_to_utf


class EmailController:
    smpt_dic = {'server': '92.63.102.41',
                'login': u'service@send2cloud.com',
                'password': u'abirvalg'}
    def __init__(self):
        self.my_pop = pop.POP3(self.smpt_dic['server'], timeout=2000)
        self.my_pop.user(self.smpt_dic['login'])
        self.my_pop.pass_(self.smpt_dic['password'])

    def __call__(self):
        self.my_pop = pop.POP3(self.smpt_dic['server'], timeout=2000)
        self.my_pop.user(self.smpt_dic['login'])
        self.my_pop.pass_(self.smpt_dic['password'])


    def sendMail(self, to, subject, text=None, html=None):

        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = self.smpt_dic['login']
        msg['To'] = to
        # Record the MIME types of both parts - text/plain and text/html.
        if text:
            part1 = MIMEText(text, 'plain')
            msg.attach(part1)
        elif html:
            part2 = MIMEText(html, 'html')
            msg.attach(part2)
            # Send the message via local SMTP server.
        s = smtplib.SMTP('mail.send2cloud.com')
        s.login(self.smpt_dic['login'].encode(), self.smpt_dic['password'].encode())
        # sendmail function takes 3 arguments: sender's address, recipient's address
        # and message to send - here it is sent as one string.
        #TODO test why i've  used raw email
        s.sendmail(self.smpt_dic['login'], to, msg.as_string())
        s.quit()
        logging.info('I have sent email to ' + to)

    def look_for_email(self):
        count, size = self.my_pop.stat()
        return count

    def get_email_list(self):
        response, mesh_num, octets = self.my_pop.list()
        mes_list = []
        print 'we have', len(mesh_num), 'messages'
        for i in range(len(mesh_num)):
            mes_dict = {'from': '', 'to': '', 'attach_name': '', 'hash_name': '', 'file': "", 'filesize': "", 'is_premium': ""}
            message = self.my_pop.retr(mesh_num[i])[1]
            mes = "\n".join(message)
            msg = email.message_from_string(mes)
            user_id = 0
            for part in msg.walk():
                if part.get("Subject"):
                    user = get_user_byName(part.get("Subject"))
                    # test, if user exists
                    if user:
                        'found user'
                        if user.user_active:
                            print 'found active user', user.user_name
                            mes_dict['to'] = part.get("Subject")
                            mes_dict['is_premium'] = user.is_premium
                            user_id = user.user_id
                        else:
                            self.sendMail(part.get("From"), 'Uknown user',
                                          'Sorry, but ' + part.get("Subject") + " user is not active.")
                            break
                    else:
                        self.sendMail(part.get("From"), 'Uknown user',
                                      'Sorry, but we don\'t know anything  about ' + part.get("Subject") + " user.")
                        break
                if part.get("From"):
                    # clear mail address
                    from_string = str(part.get("From"))[:-1].rsplit('<')
                    mes_dict['from'] = from_string[1]
                    print 'email from', mes_dict['from']

            #test for black_list
            if test_for_blacklist(mes_dict['from'], user_id):
                self.my_pop.dele(mesh_num[i])
                continue

            #test for secret code
            for part in msg.get_payload():
                user = get_user_byId(user_id)
                if user.user_secret:
                    print 'found secret code in db', user.user_secret, type (user.user_secret)
                    secret = None
                    #search for body text
                    if part.get_content_maintype() == 'multipart':
                        submes = part.get_payload()
                        for item in submes:
                            if item.get_content_type() == 'text/plain':
                                try:
                                    secret = int(item.get_payload())
                                    print 'found secret code in email', secret, type(secret)
                                except ValueError:
                                    print 'secret code in email is not digit'
                                    continue
                        if user.user_secret != secret:
                            print 'codes are not equal'
                            break
                        else:
                            print 'codes are equal'
                if part.get_filename():
                    print 'found file'
                    # test if filename is koi encoded
                    if decode_header(part.get_filename())[0][1] == 'koi8-r':
                        str_line = decode_header(part.get_filename())[0][0]
                        str_line = from_koi_to_utf(str_line)
                    else:
                        str_line = part.get_filename()
                    mes_dict['attach_name'] = str_line
                    print 'attach_name', mes_dict['attach_name']
                    mes_dict['hash_name'] = get_hash_name()
                    print 'hash_name', mes_dict['hash_name']
                    try:
                        file = open('/tmp/' + mes_dict['hash_name'], 'w+b')
                        file.write(part.get_payload(decode=True))
                        mes_dict['filesize'] = len(part.get_payload(decode=True))
                        file.close()
                    except EOFError:
                        print EOFError.message
                    mes_list.append(mes_dict.copy())
                    print mes_list
            #del message after work is done
            self.my_pop.dele(mesh_num[i])
        self.my_pop.quit()
        return mes_list