__author__ = 'shadrus'
from wtforms import Form, TextField, PasswordField
from wtforms.validators import Length, DataRequired, Email, EqualTo


class LoginForm(Form):
    user = TextField('user', validators=[Length(min=5)])
    password = PasswordField('password', validators=[DataRequired()])


class RegisterForm(Form):
    user = TextField('user', validators=[Length(min=5), DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    email = TextField('email', validators=[Email(), DataRequired()])


class ChangePasswordForm(Form):
    password = PasswordField('password', validators=[DataRequired()])
    password2 = PasswordField('password2', validators=[DataRequired(), EqualTo('password')])


class SetSecurityCode(Form):
    code = TextField('code', validators=[DataRequired()])


class EmailForm(Form):
    email = TextField('email', validators=[DataRequired(), Email()])


class DropboxCodeForm(Form):
    code = TextField('code', validators=[DataRequired()])
