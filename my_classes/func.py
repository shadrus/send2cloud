# -*- coding: utf-8 -*-

from os import path, urandom
from hashlib import sha256

__author__ = 'shadrus'


def get_hash_name():
    return urandom(16).encode('hex')


def del_illeg_chars(word):
    danger_chars = "/<>:\"|?*\\"
    new_word = ""
    print "WORD", word
    for char in word:
        if char in danger_chars:
            pass
        else:
            new_word += char
    print "NEW_WORD", new_word
    #word = word.translate(danger_chars)
    return new_word


def from_koi_to_utf(self, str_line):
    filename, fileext = path.splitext(str_line)
    print filename, fileext
    try:
        str_line = str_line.decode('koi8-r')
        str_line = str_line.encode("utf-8")
        print str_line
    except UnicodeDecodeError:
        print "Error while decoding. Use default filename"
        str_line = "upload" + fileext
    return str_line


def str_to_sha(string):
    m = sha256()
    m.update(string)
    result = m.hexdigest()
    return result


