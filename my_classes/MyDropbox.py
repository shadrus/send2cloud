import os
from dropbox.client import DropboxOAuth2FlowBase
from dao import get_dropbox_apptoken, get_user_token_from_db
from io import BytesIO
from my_classes.func import del_illeg_chars

__author__ = 'krylov'

# Include the Dropbox SDK
import dropbox


class MyDropboxClass:
    # Get your app key and secret from the Dropbox developer website

    user = {}
    flow = dropbox.client.DropboxOAuth2FlowNoRedirect(None, None)
    client = dropbox.client.DropboxClient('')

    def __init__(self, user_id):
        drop_settings = get_dropbox_apptoken()
        app_key = drop_settings.cloud_app_key
        app_secret = drop_settings.cloud_app_secret
        user_token = get_user_token_from_db(user_id, 'dropbox')
        if user_token:
            self.client = dropbox.client.DropboxClient(user_token)
        else:
            self.flow = dropbox.client.DropboxOAuth2FlowNoRedirect(app_key, app_secret)

    #Make request to Dropbox for auth code
    def MakeAppAuthReq(self, user_id):
        self.user = {"user": user_id}
        authorize_url = self.flow.start()
        return authorize_url

    def GotCode(self, code):
        result = {}
        # This will fail if the user enters an invalid authorization code
        access_token, user_id = self.flow.finish(code)
        self.client = dropbox.client.DropboxClient(access_token)
        return access_token

    def putFile(self, file_name, file_hash_name, del_after_put=True):
        if file_hash_name:
            try:
                file = open('/tmp/' + file_hash_name, 'rb')
                file_name = del_illeg_chars(file_name)
                response = self.client.put_file(file_name, file)
                if del_after_put:
                    os.remove('/tmp/' + file_hash_name)
                return True
            except EOFError:
                print EOFError.message
                return False



